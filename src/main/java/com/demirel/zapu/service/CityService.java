package com.demirel.zapu.service;

import com.demirel.zapu.dto.Location.CityDto;
import com.demirel.zapu.repository.CityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequiredArgsConstructor
public class CityService {

    private final CityRepository cityRepository;

    public List<CityDto> getAllCities() {
        return cityRepository.findAll()
            .stream()
            .map((city) -> CityDto
                .builder()
                .id(city.getId())
                .name(city.getName())
                .code(city.getCode())
                .build())
            .collect(toList());
    }

}
