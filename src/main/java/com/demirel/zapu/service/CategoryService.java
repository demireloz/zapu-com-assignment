package com.demirel.zapu.service;

import com.demirel.zapu.dto.CategoryDto;
import com.demirel.zapu.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public List<CategoryDto> getAllCategories() {
        return categoryRepository.findAll().stream()
            .map((category -> CategoryDto
                .builder()
                .id(category.getId())
                .name(category.getName()).build()
            ))
            .collect(Collectors.toList());
    }


}
