package com.demirel.zapu.service;

import com.demirel.zapu.configuration.ApplicationProperties;
import com.demirel.zapu.dto.PagedPropertiesDto;
import com.demirel.zapu.dto.PropertyDto;
import com.demirel.zapu.dto.constants.CurrencyCode;
import com.demirel.zapu.dto.exceptions.CategoryNotFoundException;
import com.demirel.zapu.dto.exceptions.CityNotFoundException;
import com.demirel.zapu.dto.exceptions.PropertyNotFoundException;
import com.demirel.zapu.mapper.PropertyMapper;
import com.demirel.zapu.model.Category;
import com.demirel.zapu.model.Property;
import com.demirel.zapu.model.location.City;
import com.demirel.zapu.repository.CategoryRepository;
import com.demirel.zapu.repository.CityRepository;
import com.demirel.zapu.repository.PropertyRepository;
import lombok.RequiredArgsConstructor;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class PropertyService {

    private final PropertyRepository propertyRepository;
    private final CityRepository cityRepository;
    private final CategoryRepository categoryRepository;
    private final PropertyMapper propertyMapper;
    private final ApplicationProperties applicationProperties;

    public PropertyDto save(PropertyDto propertyDto) {
        City city = findCityByCityId(propertyDto.getCityId());
        Category category = findCategoryByName(propertyDto.getCategoryName());
        if (propertyDto.getId() != null) {//update
            Property property = findPropertyById(propertyDto.getId());
            property.setCity(city);
            property.setCurrency(CurrencyCode.currencyCode(propertyDto.getCurrency()));
            property.setPrice(property.getPrice());
            property.setTitle(propertyDto.getTitle());
            property.setCategory(category);
            Property saved = propertyRepository.save(property);
            return propertyMapper.mapToDto(saved);
        } else {
            return propertyMapper
                .mapToDto(propertyRepository.save(propertyMapper.mapToEntity(propertyDto, city, category)));
        }
    }

    @Cacheable(value = "properties")
    public PropertyDto findById(Long propertyId) {
        return propertyMapper.mapToDto(findPropertyById(propertyId));
    }

    public PagedPropertiesDto resolveParamsAndSearch(String categoryName, int page, int size) {
        return resolveParamsAndSearch(categoryName, null, page, size);
    }

    public PagedPropertiesDto resolveParamsAndSearch(String categoryName, String cityName, int page, int size) {
        Long categoryId = findCategoryByName(categoryName).getId();
        List<Long> cityIds = null;
        if (cityName != null) {
            cityIds = Arrays.asList(findCityByName(cityName).getId());

        }
        return searchProperty(categoryId, cityIds, page, size);
    }

    public PagedPropertiesDto searchProperty(Long categoryId, List<Long> cityIds, int page, int size) {
        return getPropertyListByPage(categoryId, cityIds, page, size);
    }

    private PagedPropertiesDto getPropertyListByPage(Long categoryId, List<Long> cityIds, int page, int size) {
        PageRequest paging = PageRequest.of(page, size);
        Page<Property> properties;
        if (Objects.nonNull(categoryId) && cityIds != null) {
            properties = propertyRepository.findAllByCategoryAndCityIds(categoryId, cityIds, paging);
        } else if (Objects.nonNull(categoryId)) {
            properties = propertyRepository.findAllByCategoryId(categoryId, paging);
        } else {
            properties = propertyRepository.findAll(paging);
        }

        String rootUrl = rootUrlGenerator(categoryId, cityIds, page);
        return getPagedPropertiesDto(size, properties, rootUrl);
    }

    private String rootUrlGenerator(Long categoryId, List<Long> cityIds, int page) {
        String rootUrl = applicationProperties.getSiteUrl();
        URIBuilder urlBuilder = new URIBuilder();
        if (generateRegularUrlIfNotPossibleSEO(categoryId, cityIds, page, rootUrl, urlBuilder)) {
            return urlBuilder.toString();
        }

        //generate seo friendly url below
        Category category = findCategoryById(categoryId);
        List<String> pathSegments = new ArrayList<>();
        urlBuilder = new URIBuilder();
        urlBuilder.setScheme("https");
        urlBuilder.setHost(rootUrl);
        pathSegments.add(category.getName());

        if (Objects.nonNull(cityIds) && cityIds.size() == 1) {
            City city = findCityByCityId(cityIds.get(0));
            pathSegments.add(city.getName());
        }
        urlBuilder.setPathSegments(pathSegments);
        if (page != 0)
            urlBuilder.addParameter("page", page + "");

        return urlBuilder.toString();
    }

    private boolean generateRegularUrlIfNotPossibleSEO(Long categoryId, List<Long> cityIds, int page, String rootUrl, URIBuilder urlBuilder) {
        urlBuilder.setScheme("https");
        urlBuilder.setHost(rootUrl);
        if (Objects.isNull(categoryId)) {
            urlBuilder.setPath("/arama");
            if (page != 0)
                urlBuilder.addParameter("page", page + "");
            return true;
        }

        if (Objects.nonNull(cityIds) && cityIds.size() > 1) {
            urlBuilder.clearParameters();
            urlBuilder.removeQuery();
            urlBuilder.setScheme("https");
            urlBuilder.setHost(rootUrl);
            urlBuilder.setPath("/arama");
            urlBuilder.addParameter("category", categoryId + "");
            for (Long cityId : cityIds) {
                urlBuilder.addParameter("city", cityId + "");
            }
            if (page != 0)
                urlBuilder.addParameter("page", page + "");
            return true;
        }
        return false;
    }

    private PagedPropertiesDto getPagedPropertiesDto(int size, Page<Property> properties, String rootUrl) {
        List<PropertyDto> propertyDtoList = properties.getContent().stream().map((property) -> PropertyDto.builder()
            .id(property.getId())
            .price(property.getPrice())
            .currency(property.getCurrency().getCode())
            .cityId(property.getCity().getId())
            .cityName(property.getCity().getName())
            .categoryName(property.getCategory().getName())
            .title(property.getTitle())
            .build()
        ).collect(toList());

        return PagedPropertiesDto.builder()
            .propertyDtoList(propertyDtoList)
            .pageSize(size)
            .currenPage(properties.getNumber())
            .totalItems(properties.getTotalElements())
            .totalPage(properties.getTotalPages())
            .rootUrl(rootUrl)
            .build();
    }

    private City findCityByCityId(Long cityId) {
        return cityRepository.findById(cityId).orElseThrow(
            () -> new CityNotFoundException(
                String.format("city not found with given cityId {}", cityId)
            )
        );
    }

    private City findCityByName(String name) {
        return cityRepository.findByName(name).orElseThrow(
            () -> new CityNotFoundException(
                String.format("City name has not found {}", name)));
    }

    private Property findPropertyById(Long propertyId) {
        return propertyRepository
            .findById(propertyId).orElseThrow(
                () -> new PropertyNotFoundException(
                    String.format("property not found with given id {}", propertyId)
                ));
    }

    private Category findCategoryById(Long categoryId) {
        return categoryRepository.findById(categoryId)
            .orElseThrow(
                () -> new CategoryNotFoundException(
                    String.format("categoryId has not found {}", categoryId)));
    }

    private Category findCategoryByName(String name) {
        return categoryRepository.findByName(name)
            .orElseThrow(
                () -> new CategoryNotFoundException(
                    String.format("category name has not found {}", name)));
    }

}
