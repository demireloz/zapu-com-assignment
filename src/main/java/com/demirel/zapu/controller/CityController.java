package com.demirel.zapu.controller;

import com.demirel.zapu.service.CityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
@RequiredArgsConstructor
public class CityController {

    private final CityService cityService;

    @GetMapping("/api/cities")
    public ResponseEntity<?> getAllCities() {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(cityService.getAllCities());
    }


}
