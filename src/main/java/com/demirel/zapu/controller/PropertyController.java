package com.demirel.zapu.controller;

import com.demirel.zapu.dto.PropertyDto;
import com.demirel.zapu.service.PropertyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class PropertyController {

    private final PropertyService propertyService;

    @PostMapping("/api/property")
    public ResponseEntity<?> save(@RequestBody PropertyDto propertyDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(propertyService.save(propertyDto));
    }

    @PutMapping("/api/property")
    public ResponseEntity<?> update(@RequestBody PropertyDto propertyDto) {// to have idempotency I separated the methods...
        return ResponseEntity.status(HttpStatus.OK).body(propertyService.save(propertyDto));
    }

    @GetMapping("/detay/{propertyId}")
    public ResponseEntity<?> getPropertyById(@PathVariable("propertyId") Long propertyId) {
        return ResponseEntity.status(HttpStatus.OK).body(propertyService.findById(propertyId));
    }

    @GetMapping("/arama")
    public ResponseEntity<?> searchProperty(
        @RequestParam(defaultValue = "0", name = "page") int page,
        @RequestParam(defaultValue = "5", name = "size") int size,
        @RequestParam(required = false, name = "categoryId") Long categoryId,
        @RequestParam(required = false, name = "city") List<Long> city
    ) {
        return ResponseEntity.status(HttpStatus.OK).body(propertyService.searchProperty(categoryId, city, page, size));
    }

    @GetMapping("/{categoryName}/{cityName}")
    public ResponseEntity<?> searchProperty(@PathVariable(value = "categoryName", required = true) String categoryName,
                                            @PathVariable(value = "cityName", required = false) String cityName,
                                            @RequestParam(defaultValue = "0", name = "page") int page,
                                            @RequestParam(defaultValue = "5", name = "size") int size) {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(propertyService.resolveParamsAndSearch(categoryName, cityName, page, size));
    }

    @GetMapping("/{categoryName}")
    public ResponseEntity<?> searchProperty(@PathVariable(value = "categoryName", required = true) String categoryName,
                                            @RequestParam(defaultValue = "0", name = "page") int page,
                                            @RequestParam(defaultValue = "5", name = "size") int size) {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(propertyService.resolveParamsAndSearch(categoryName, page, size));
    }

}
