package com.demirel.zapu.mapper;

import com.demirel.zapu.dto.PropertyDto;
import com.demirel.zapu.dto.constants.CurrencyCode;
import com.demirel.zapu.model.Category;
import com.demirel.zapu.model.Property;
import com.demirel.zapu.model.location.City;
import org.springframework.stereotype.Component;

@Component
public class PropertyMapper {

    public PropertyDto mapToDto(Property property) {
        return PropertyDto.builder()
            .id(property.getId())
            .title(property.getTitle())
            .cityId(property.getCity().getId())
            .currency(property.getCurrency().getCode())
            .price(property.getPrice())
            .categoryName(property.getCategory().getName())
            .cityName(property.getCity().getName())
            .build();
    }

    public Property mapToEntity(PropertyDto propertyDto, City city, Category category) {
        return Property.builder()
            .id(propertyDto.getId())
            .city(city)
            .title(propertyDto.getTitle())
            .price(propertyDto.getPrice())
            .currency(CurrencyCode.currencyCode(propertyDto.getCurrency()))
            .category(category)
            .build();
    }
}
