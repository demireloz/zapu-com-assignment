package com.demirel.zapu;

import com.demirel.zapu.configuration.ApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@EnableConfigurationProperties({ApplicationProperties.class})
public class ZapuAssignmentApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ZapuAssignmentApplication.class);
        addDefaultProfile(app);
        Environment env = app.run(args).getEnvironment();
    }

    public static void addDefaultProfile(SpringApplication app) {
        Map<String, Object> defProperties = new HashMap();
        defProperties.put("spring.profiles.default", "dev");
        app.setDefaultProperties(defProperties);
    }

}
