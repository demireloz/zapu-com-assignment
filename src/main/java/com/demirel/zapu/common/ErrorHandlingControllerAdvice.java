package com.demirel.zapu.common;

import com.demirel.zapu.dto.exceptions.CategoryNotFoundException;
import com.demirel.zapu.dto.exceptions.CityNotFoundException;
import com.demirel.zapu.dto.exceptions.PropertyNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@ControllerAdvice
public class ErrorHandlingControllerAdvice {

    @ExceptionHandler(PropertyNotFoundException.class)
    ResponseEntity<String> categoryNotFoundExceptionHandler(final PropertyNotFoundException ex, final HttpServletResponse response) {
        log.error(ex.getMessage(), ex);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        return ResponseEntity
            .status(HttpStatus.NOT_FOUND)
            .body("PropertyNotFoundException error occurred!!!");
    }

    @ExceptionHandler(CityNotFoundException.class)
    ResponseEntity<String> categoryNotFoundExceptionHandler(final CityNotFoundException ex, final HttpServletResponse response) {
        log.error(ex.getMessage(), ex);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        return ResponseEntity
            .status(HttpStatus.NOT_FOUND)
            .body("CityNotFoundException error occurred!!!");
    }

    @ExceptionHandler(CategoryNotFoundException.class)
    ResponseEntity<String> categoryNotFoundExceptionHandler(final CategoryNotFoundException ex, final HttpServletResponse response) {
        log.error(ex.getMessage(), ex);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        return ResponseEntity
            .status(HttpStatus.NOT_FOUND)
            .body("CategoryNotFoundException error occurred!!!");
    }

    @ExceptionHandler(Throwable.class)
    ResponseEntity<String> defaultExceptionHandler(final Throwable ex, final HttpServletResponse response) {
        log.error(ex.getMessage(), ex);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        return ResponseEntity
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body("Unknown/Unhandled error occurred!!!");
    }

}
