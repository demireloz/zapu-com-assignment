package com.demirel.zapu.dto.constants;


import lombok.AllArgsConstructor;
import lombok.Getter;


public enum CurrencyCode {

    TL("TL"), EURO("EURO"), DOLLAR("DOLLAR");

    @Getter
    private final String code;

    CurrencyCode(String code) {
        this.code = code;
    }

    public static CurrencyCode currencyCode(String code) {
        for (CurrencyCode currencyCode : values()) {
            if (currencyCode.code.equals(code)) {
                return currencyCode;
            }
        }
        throw new RuntimeException("Currency code has not found");
    }


}
