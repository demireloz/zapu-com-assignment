package com.demirel.zapu.dto.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LocationType {

    public static final String CITY = "CITY";
    public static final String MAYOR = "MAYOR";
    public static final String DISTRICT = "DISTRICT";

}
