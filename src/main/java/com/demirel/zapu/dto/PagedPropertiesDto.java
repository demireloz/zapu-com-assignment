package com.demirel.zapu.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PagedPropertiesDto {

    @JsonProperty("properties")
    private List<PropertyDto> propertyDtoList;
    @JsonProperty("page")
    private Integer currenPage;
    @JsonProperty("pageSize")
    private Integer pageSize;
    @JsonProperty("count")
    private Long totalItems;
    @JsonProperty("totalPage")
    private Integer totalPage;
    @JsonProperty("rootUrl")
    private String rootUrl;

}
