package com.demirel.zapu.dto;


import com.demirel.zapu.dto.constants.LocationType;
import lombok.*;

@Getter
@AllArgsConstructor
public class Location {

    private Long id;
    private String name;
    private String code;
    private Long parentId;
    private String locationType;

    public static class CityDto extends Location {
        @Builder
        public CityDto(Long id, String name, String code, Long parentId) {
            super(id, name, code, parentId, LocationType.CITY);
        }
    } // istanbul

    static class MayorDto extends Location {
        @Builder
        public MayorDto(Long id, String name, String code, Long parentId) {
            super(id, name, code, parentId, LocationType.MAYOR);
        }
    } // beyoglu

    static class DistrictDto extends Location {
        @Builder
        public DistrictDto(Long id, String name, String code, Long parentId) {
            super(id, name, code, parentId, LocationType.DISTRICT);
        }
    } // Cihangir

}
