package com.demirel.zapu.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
public class PropertyDto implements Serializable {

    private Long id;
    private String title;
    @JsonProperty("city")
    private String cityName;
    private Long cityId;
    private BigDecimal price;
    private String currency;
    private String categoryName;

}
