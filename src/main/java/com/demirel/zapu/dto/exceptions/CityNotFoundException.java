package com.demirel.zapu.dto.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CityNotFoundException extends RuntimeException {

    public CityNotFoundException(String message) {
        super(message);
        log.error(message);
    }
}
