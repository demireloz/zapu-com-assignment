package com.demirel.zapu.model.location;

import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import static com.demirel.zapu.dto.constants.LocationType.DISTRICT;

@NoArgsConstructor
@Entity
@DiscriminatorValue(DISTRICT)
public class District extends Location {

}
