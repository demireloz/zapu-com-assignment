package com.demirel.zapu.model.location;

import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import static com.demirel.zapu.dto.constants.LocationType.MAYOR;

@NoArgsConstructor
@Entity
@DiscriminatorValue(MAYOR)
public class Mayor extends Location {

}
