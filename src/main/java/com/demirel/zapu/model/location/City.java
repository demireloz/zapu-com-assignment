package com.demirel.zapu.model.location;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import static com.demirel.zapu.dto.constants.LocationType.CITY;


@NoArgsConstructor
@Entity
@DiscriminatorValue(CITY)
public class City extends Location {

}





