package com.demirel.zapu.repository;

import com.demirel.zapu.model.Category;
import com.demirel.zapu.model.Property;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    Optional<Category> findByName(String name);
}
