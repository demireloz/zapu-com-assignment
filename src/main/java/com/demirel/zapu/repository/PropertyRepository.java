package com.demirel.zapu.repository;

import com.demirel.zapu.model.Property;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PropertyRepository extends JpaRepository<Property, Long> {
    Page<Property> findAllByCategoryId(Long categoryId, Pageable pageable);

    @Query("select p from Property p where p.category.id=:categoryId and p.city.id in :cityIds")
    Page<Property> findAllByCategoryAndCityIds(@Param("categoryId") Long categoryId,
                                               @Param("cityIds") List<Long> cityIds,
                                               Pageable pageable
    );


}
