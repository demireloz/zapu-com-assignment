package com.demirel.zapu.repository;

import com.demirel.zapu.model.location.Mayor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MayorRepository extends JpaRepository<Mayor, Long> {
    //ilce repository
}
