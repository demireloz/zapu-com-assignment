package com.demirel.zapu.repository;

import com.demirel.zapu.model.location.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CityRepository extends JpaRepository<City, Long> { // istanbul

    Optional<City> findByName(String name);

}
