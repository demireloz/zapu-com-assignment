package com.demirel.zapu.repository;

import com.demirel.zapu.model.location.District;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DistrictRepository extends JpaRepository<District, Long> {
}
