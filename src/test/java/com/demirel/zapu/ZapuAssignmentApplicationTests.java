package com.demirel.zapu;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.StringJoiner;

@SpringBootTest
class ZapuAssignmentApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void whenMergingJoiners_thenReturnMerged() {

        StringJoiner rgbJoiner = new StringJoiner(
            ",", "PREFIX", "SUFFIX");
        StringJoiner cmybJoiner = new StringJoiner(
            "-", "PREFIX", "SUFFIX");

        rgbJoiner.add("Red")
            .add("Green")
            .add("Blue");


        System.out.println(rgbJoiner.toString());
    }

}
