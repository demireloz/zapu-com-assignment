package com.demirel.zapu.service;

import com.demirel.zapu.configuration.ApplicationProperties;
import com.demirel.zapu.dto.PagedPropertiesDto;
import com.demirel.zapu.dto.PropertyDto;
import com.demirel.zapu.dto.constants.CurrencyCode;
import com.demirel.zapu.dto.exceptions.CityNotFoundException;
import com.demirel.zapu.dto.exceptions.PropertyNotFoundException;
import com.demirel.zapu.mapper.PropertyMapper;
import com.demirel.zapu.model.Category;
import com.demirel.zapu.model.Property;
import com.demirel.zapu.model.location.City;
import com.demirel.zapu.repository.CategoryRepository;
import com.demirel.zapu.repository.CityRepository;
import com.demirel.zapu.repository.PropertyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PropertyServiceTest {

    @InjectMocks
    private PropertyService propertyService;
    @Mock
    private PropertyRepository propertyRepository;
    @Mock
    private CityRepository cityRepository;
    @Mock
    private CategoryRepository categoryRepository;
    @Spy
    private PropertyMapper propertyMapper;
    @Mock
    private ApplicationProperties applicationProperties;

    @Test
    void shouldResolveParamsAndSearchReturnPaginatedResult() {
        PropertyDto propertyDto = PropertyDto.builder() // no id on
            .cityId(1l)
            .categoryName("konut")
            .currency("TL")
            .price(BigDecimal.TEN)
            .title("title")
            .cityName("ANKARA")
            .build();

        Integer page = 0;
        Integer size = 2;

        PageRequest paging = PageRequest.of(page, size);

        City city = getFakeCity();
        Category category = getFakeCategory();
        Property property = propertyMapper.mapToEntity(propertyDto, city, category); //getFakeProperty(city, category);
        property.setId(123l);

        Page<Property> pages = new PageImpl(Arrays.asList(property));


        when(categoryRepository.findByName(propertyDto.getCategoryName())).thenReturn(Optional.ofNullable(category));
        when(categoryRepository.findById(category.getId())).thenReturn(Optional.ofNullable(category));
        when(propertyRepository.findAllByCategoryId(category.getId(), paging)).thenReturn(pages);
        when(applicationProperties.getSiteUrl()).thenReturn("zapu.com");

        PagedPropertiesDto pagedPropertiesDto = propertyService.resolveParamsAndSearch(category.getName(), page, size);

        assertThat(pagedPropertiesDto.getPropertyDtoList().size()).isEqualTo(pages.getTotalElements());
        assertThat(pagedPropertiesDto.getCurrenPage()).isEqualTo(page);

    }

    @Test
    void shouldGetPropertyFindById() {
        City city = getFakeCity();
        Category category = getFakeCategory();
        Property property = getFakeProperty(city, category); //getFakeProperty(city, category);
        Long propertyId = property.getId();

        when(propertyRepository.findById(propertyId)).thenReturn(Optional.ofNullable(property));

        PropertyDto propertyDto = propertyService.findById(propertyId);

        assertThat(propertyDto.getCityName()).isEqualTo(property.getCity().getName());
    }

    @Test
    void shouldThrowExceptionWhenPropertyNotExist() {
        assertThrows(PropertyNotFoundException.class, () -> {
            propertyService.findById(123l);
        });
    }

    @Test
    void shouldSaveThrowExceptionWhenCityHasNotFound() {
        PropertyDto propertyDto = PropertyDto.builder() // no id on
            .cityId(1l)
            .categoryName("konut")
            .currency("TL")
            .price(BigDecimal.TEN)
            .title("title")
            .cityName("ANKARA")
            .build();
        assertThrows(CityNotFoundException.class, () -> {
            propertyService.save(propertyDto);
        });
    }

    @Test
    void shouldSavePropertySuccessfully() {
        PropertyDto propertyDto = PropertyDto.builder() // no id on
            .cityId(1l)
            .categoryName("konut")
            .currency("TL")
            .price(BigDecimal.TEN)
            .title("title")
            .cityName("ANKARA")
            .build();

        City city = getFakeCity();
        Category category = getFakeCategory();
        Property property = propertyMapper.mapToEntity(propertyDto, city, category); //getFakeProperty(city, category);
        property.setId(123l);

        when(cityRepository.findById(propertyDto.getCityId()))
            .thenReturn(Optional.ofNullable(city));

        when(categoryRepository.findByName(propertyDto.getCategoryName()))
            .thenReturn(Optional.ofNullable(category));

        when(propertyRepository.save(any())).thenReturn(property);

        PropertyDto dto = propertyService.save(propertyDto);

        assertThat(dto.getId()).isNotNull();
    }

    @Test
    void shouldUpdatePropertySuccessfully() {
        PropertyDto propertyDto = PropertyDto.builder().id(12l)
            .cityId(1l)
            .categoryName("konut")
            .currency("TL")
            .price(BigDecimal.TEN)
            .title("title")
            .cityName("ANKARA")
            .build();
        City city = getFakeCity();
        Category category = getFakeCategory();
        Property property = getFakeProperty(city, category);

        when(cityRepository.findById(propertyDto.getCityId())).thenReturn(Optional.ofNullable(city));
        when(categoryRepository.findByName(propertyDto.getCategoryName())).thenReturn(Optional.ofNullable(category));
        when(propertyRepository.findById(propertyDto.getId())).thenReturn(Optional.ofNullable(property));
        when(propertyRepository.save(any())).thenReturn(property);

        PropertyDto saved = propertyService.save(propertyDto);

        assertThat(saved.getTitle()).isEqualTo(propertyDto.getTitle());
    }

    private Property getFakeProperty(City city, Category category) {
        Property property = new Property();
        property.setId(1l);
        property.setTitle("property1");
        property.setCity(city);
        property.setPrice(BigDecimal.TEN);
        property.setCurrency(CurrencyCode.TL);
        property.setCategory(category);
        return property;
    }

    private Category getFakeCategory() {
        Category category = new Category();
        category.setId(1l);
        category.setName("konut");
        return category;
    }

    private City getFakeCity() {
        City city = new City();
        city.setId(1l);
        city.setCode("06");
        city.setName("ANKARA");
        return city;
    }


}
