package com.demirel.zapu.service;

import com.demirel.zapu.dto.CategoryDto;
import com.demirel.zapu.model.Category;
import com.demirel.zapu.repository.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CategoryServiceTest {

    @InjectMocks
    private CategoryService categoryService;

    @Mock
    private CategoryRepository categoryRepository;

    @Test
    void shouldGetAllCategories() {
        Category fakeCategory = getFakeCategory();

        when(categoryRepository.findAll()).thenReturn(Arrays.asList(fakeCategory));

        List<CategoryDto> allCategories = categoryService.getAllCategories();

        assertThat(allCategories.get(0).getName()).isEqualTo(fakeCategory.getName());
        assertThat(allCategories.get(0).getId()).isEqualTo(fakeCategory.getId());
    }

    private Category getFakeCategory() {
        Category category = new Category();
        category.setId(1l);
        category.setName("konut");
        return category;
    }

}
