package com.demirel.zapu.service;

import com.demirel.zapu.dto.Location.CityDto;
import com.demirel.zapu.model.location.City;
import com.demirel.zapu.repository.CityRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CityServiceTest {

    @InjectMocks
    private CityService cityService;

    @Mock
    private CityRepository cityRepository;

    @Test
    void shouldGetAllCities() {
        City fakeCity = getFakeCity();
        when(cityRepository.findAll()).thenReturn(Arrays.asList(fakeCity));

        List<CityDto> allCities = cityService.getAllCities();

        assertThat(allCities.get(0).getCode()).isEqualTo(fakeCity.getCode());
        assertThat(allCities.get(0).getName()).isEqualTo(fakeCity.getName());
    }

    private City getFakeCity() {
        City city = new City();
        city.setId(1l);
        city.setCode("06");
        city.setName("ANKARA");
        return city;
    }

}
