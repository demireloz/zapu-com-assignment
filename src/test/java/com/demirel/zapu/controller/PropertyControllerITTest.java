package com.demirel.zapu.controller;

import com.demirel.zapu.dto.PropertyDto;
import com.demirel.zapu.mapper.PropertyMapper;
import com.demirel.zapu.model.Property;
import com.demirel.zapu.repository.PropertyRepository;
import com.demirel.zapu.utils.TestProfile;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;

import static com.demirel.zapu.utils.TestHelper.asJsonString;
import static org.hamcrest.Matchers.notNullValue;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestProfile
@AutoConfigureMockMvc
class PropertyControllerITTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PropertyRepository propertyRepository;

    @Autowired
    private PropertyMapper propertyMapper;

    @Test
    void shouldGiveAnErrorOnSaveWithNotExistingCity() throws Exception {
        String url = "/v1/api/property";
        PropertyDto propertyDto = dummyPropertyDto();
        propertyDto.setCityName("NO_Where");
        propertyDto.setCityId(8999l);

        mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(propertyDto))
            )
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(content().string("CityNotFoundException error occurred!!!"));
        ;
    }

    @Test
    void shouldGiveAnErrorOnSaveWithNotExistingCategory() throws Exception {
        String url = "/v1/api/property";
        PropertyDto propertyDto = dummyPropertyDto();
        propertyDto.setCategoryName("not_existing_category");

        mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(propertyDto))
            )
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(content().string("CategoryNotFoundException error occurred!!!"));
    }

    @Test
    void shouldSavePropertySuccessfully() throws Exception {
        String url = "/v1/api/property";
        PropertyDto propertyDto = dummyPropertyDto();

        mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(propertyDto))
            )
            .andDo(print())
            .andExpect(jsonPath("$.id", notNullValue()))
            .andExpect(status().isCreated());
    }

    @Test
    void shouldUpdatePropertySuccessfully() throws Exception {
        String url = "/v1/api/property";
        Property property = propertyRepository.findById(1l).get();
        PropertyDto propertyDto = propertyMapper.mapToDto(property);
        propertyDto.setTitle("updated");

        mockMvc.perform(MockMvcRequestBuilders
                .put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(propertyDto))
            )
            .andDo(print())
            .andExpect(jsonPath("$.title").value("updated"))
            .andExpect(status().isOk());
    }

    @Test
    void shouldGetDetailByPropertyId() throws Exception {
        String url = String.format("/v1/detay/%d", 1l);
        Property property = propertyRepository.findById(1l).get();


        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(jsonPath("$.title").value(property.getTitle()))
            .andExpect(status().isOk());
    }

    @Test
    void shouldGetDetailByPropertyIdThrowExceptionOnNotExistingId() throws Exception {
        String url = String.format("/v1/detay/%d", 100997l);
        Property property = propertyRepository.findById(1l).get();

        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(content().string("PropertyNotFoundException error occurred!!!"));

    }

    @Test
    void shouldSearchReturn200() throws Exception {
        int totalProperty = propertyRepository.findAll().size();
        String url = "/v1/arama";
        int page = 0;
        int size = 5;
        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .queryParam("page", "" + page)
                .queryParam("size", "" + size)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(jsonPath("$.count").value(totalProperty))
            .andExpect(jsonPath("$.totalPage").value((totalProperty / size) + 1))
            .andExpect(status().isOk());
    }

    @Test
    void shouldGetDataOnSearchWithSeoFriendlyUrl() throws Exception {
        int totalProperty = propertyRepository.findAll().size();
        String url = "/v1/konut/ANKARA";
        String rootUrl = "https://zapu.com/konut/ANKARA";
        int page = 0;
        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .queryParam("page", "" + page)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(jsonPath("$.rootUrl").value(rootUrl))
            .andExpect(status().isOk());
    }

    @Test
    void shouldGetByCategoryAndCityIds() throws Exception {
        String url = "/v1/arama";
        String rootUrl = "https://zapu.com/arama?category=1&city=1&city=2";
        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .queryParam("categoryId", "1")
                .queryParam("city", "1", "2")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(jsonPath("$.rootUrl").value(rootUrl))
            .andExpect(status().isOk());
    }


    private PropertyDto dummyPropertyDto() {
        return PropertyDto.builder()
            .title("Test product")
            .cityName("ANKARA")
            .currency("TL")
            .price(BigDecimal.TEN)
            .categoryName("ticari")
            .cityId(1l)
            .build();
    }


}
