This REST API designed to act as a server part of the `zapu.com assignment solution` as a proof of concept.

User session management, Authorization and Authentication is neglected on purpose due to demonstration of p.o.c

End-points served by this API can be easily watched by navigating following url: http://localhost:8080/swagger-ui.html

IT and Unit tests are written under the test folder

You can compile it via

```bash
./mvnw -U clean compile package
```

It creates a `app.jar` file in the "target" directory. You can run it directly:

```bash
java -jar target/app.jar
```

If you want to create a docker image:

```bash
docker build . -t ozgurclub/app
```

## Solution Description
- Embedded cache is used to reduce the load of detail pages from within the application.
- 2 different 'yml' files are created for dev and prod profiles in the application. DEV=H2, PROD=Postgresql
- For production profile "Postgresql" database configured on docker-compose.yml

## CI pipeline

CI pipeline configuration initialized  on .gitlab.yml

## Improvement areas:
- Code coverage
- Documentation
- Interface segregation
- Validation Api
- Log enhancements
- Continuous Deployment
